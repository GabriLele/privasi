> **!!!COME USARE LA LISTA!!!**
> Come dice il detto: "chi va piano, va sano e va lontano". L'obiettivo non è arrivare primi (non c'è nessun premio), bensì capire *cosa* fa il singolo punto e il *perché* è stato suggerito. Cliccateci su e si apriranno le istruzioni con una breve spiegazione (ma anche lunga, per chi vuole approfondire). Prendetevi il tempo che vi serve :)

> **!!!come NON usare la lista!!!**
> Seguire ogni punto senza capirne il motivo. Chiudercisi in maniera compulsiva finché non si ha finito. Saltare tra i livelli

**!!! La lista non è ultimata: chiunque è libero di contribuire come spiegato [QUI](https://gitlab.com/etica-digitale/privasi/blob/master/FAQ.md#come-posso-contribuire-alla-documentazione) !!!**  

**!!! I lavori procedono quotidianamente: se siete arrivati all'ultimo punto cliccabile, vi invitiamo a fare un salto una volta a settimana per trovare nuovi punti consultabili. [QUI](https://gitlab.com/etica-digitale/privasi/commits/master) il registro di tutte le modifiche !!!**  

<br>

**Livello 0: Comprendere**
- [Introduzione: cosa me ne importa della privacy?](Spiegazioni/L0-0___Intro.md)
- [Google - un esempio: vedere con i propri occhi i dati che abbiamo acconsentito di condividere](Spiegazioni/L0-1___Google.md)

&nbsp;

**Livello 1: Limitare i danni**
- [Rimozione consenso](Spiegazioni/L1-0___Activity-Deletion.md)
- [Password 101](Spiegazioni/L1-1___Passwords.md)
- [Chat pubbliche: ~~Messenger~~ Telegram ](Spiegazioni/L1-2___Telegram.md)
- [Conversazioni sensibili pt.1: Smart TV e assistenti personali (Internet delle Cose)](Spiegazioni/L1-3___IoT.md)
- [Conversazione sensibili pt.2: Signal incontra Whatsapp](Spiegazioni/L1-4___Signal.md)
- [Motori di ricerca: al di là di Google](Spiegazioni/L1-5___Search-Engine.md)
- [LibreOffice](Spiegazioni/L1-6___Libre-Office.md)
- [Gesti quotidiani](Spiegazioni/L1-7___Daily-Habits.md)
- [Fuga dal superfluo](Spiegazioni/L1-8___Accounts-deletion.md)

&nbsp;

**Livello 2: Mezzi per muoversi**
- [Cookie: briciole di internet](Spiegazioni/L2-0___Cookies.md)
- [Non nel mio nome pt.1: F-Droid, lo store trasparente](Spiegazioni/L2-1___F-Droid.md)
- [Non nel mio nome pt.2: ~~Chrome~~ Bromite e Firefox, finestre della rete](Spiegazioni/L2-2___Firefox.md)
- [Mail temporanee](Spiegazioni/L2-3___10minute.md)
- [~~YouTube~~ Video in un riflesso](Spiegazioni/L2-4___YouTube.md)
- [~~Google Maps~~ OpenStreetMap](Spiegazioni/L2-5___OSM.md)
- [WebApps: social a compartimenti stagni](Spiegazioni/L2-6___WebApps.md)
- [Esodo](Spiegazioni/L2-7___Exodus.md)

&nbsp;

**Livello 3: Compartimentare**
- [Compartiche?](Spiegazioni/L3-0___Compartmentalization.md)
- [Social e Professionale: profili Mozilla](Spiegazioni/L3-1___Profiles.md)
- [Personale: Tor, cipolle digitali](Spiegazioni/L3-2___Tor.md)
- uBlock Origin
- Mail separate

&nbsp;

**Livello 4: Pilastri del quotidiano**
- ~~Spotify~~
- ~~WhatsApp~~
- ~~Discord~~
- Social: tra il fediverso e il nulla
- Streaming on-demand: dimmi cosa guardi e ti dirò chi sei
- Alternative etiche: una lista

&nbsp;

**Livello 5: Chi fa da sé**
- Freemium e nuovi modelli di business
- DNS
- ROM telefono
- GNU/Linux: il sistema operativo libero
