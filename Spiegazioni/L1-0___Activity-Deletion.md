# Rimozione consenso

>La prima modalità di Google per generare soldi è la pubblicità¹. Per aumentare le probabilità di vendita, l'azienda profila l'utenza per ottenere quante più informazioni possibili sul suo conto come dimostrabile [qui](https://myactivity.google.com/myactivity) (dovete essere connessi con l'account Google) e [qui](https://www.google.com/maps/timeline). Tuttavia, disattivando determinate impostazioni come illustrato di seguito, si potrà continuare ad usare il servizio, eliminando i dati già raccolti e annullando (o limitando, a discrezione dell'utente) la raccolta di quelli successivi. Il tutto senza futuri problemi di utilizzo.

## In parole semplici
Immaginate una mattina di entrare in una biblioteca privata. State 10 minuti, chiedete se c'è un libro, lo prendete in prestito lasciando i vostri dati, e ve ne andate.
  
Immaginate invece ora che quando entrate nella biblioteca, un commesso vi fermi al'entrata, vi porga un questionario e vi dica: "per entrare deve compilare questo foglio con tutti i suoi dati e accettare che venga monitorato ogni suo movimento dalle telecamere. Lo facciamo per migliorare la sua esperienza". Se accettate, entrate. Se non accettate, sapete dov'è l'uscita².  
  
Raccogliere informazioni è una cosa naturale che ci permette di sopravvivere, instaurare rapporti e, in generale, muoverci per il mondo. Per esempio, se la mamma chiede a suo figlio cosa vuole mangiare, non lo fa perché lo tiene sotto torchio, bensì perché vuole venirgli in contro cucinando qualcosa che gli piaccia. Di conseguenza, ottenere informazioni non è di per sé sbagliato; anzi, è vitale. Al contrario, se gli mettesse una cimice nello zaino, sarebbe tutta un'altra storia. Il problema quindi è *come* le informazioni vengono ottenute, *quante* e *per quale scopo*. La differenza tra le due biblioteche infatti è enorme. 
  
Google è la seconda biblioteca: quando create un account e accettate i termini e condizioni d'utilizzo, gli state dando ogni permesso possibile e immaginabile per profilarvi nel tempo (come le telecamere nella biblioteca) e il suo impero è così grande che dove vi girate girate, ci sono servizi Google. Salvo casi eccezionali questi dati (le vostre abitudini) restano a Google, tuttavia il problema rimane un altro: *perché* condividere con una macchina cose che, se sommate, non diremmo neanche alla persona più cara? Cosa significa "migliorare la tua esperienza"? Significa dare app gratuite in cambio di schedare continuamente noi e chi ci sta intorno? Sembra abbastanza assurdo, soprattutto considerando che quelle stesse app possono essere sviluppate in modo etico.
  
È come se l'ottico vi regalasse un paio di occhiali alla condizione di montarci sopra una microtelecamera dotata di microfono. Per registrare tutte le cose che vedete, dite, fate e sentite. Nessuno vi sta dicendo di non farlo, ma è dimostrato che chi sa di essere monitorato, inizia ad autocensurarsi³. Prova del 9: googlereste senza problemi "inno ISIS"? Magari volevate semplicemente capire quale fosse il loro inno, ma al tempo stesso avete avuto il timore che "qualcuno" potesse pensare di aver davanti un terrorista. Oppure googlereste "sito porno"? Beh, questo è certo che lo fate: ma nella modalità incognito⁴ ;)
  
Quindi cosa fare? Gli occhiali già ce li abbiamo e buttarli di punto in bianco in questo caso sarebbe sconsiderato (ricordiamoci che c'è gran parte della nostra vita, probabilmente anche lavorativa). Iniziamo dalla cosa più semplice allora: cancelliamo le vecchie registrazioni e mettiamo un bel pezzo di scotch sulla telecamera!


> Per approfondire il discorso, potete poi consultare qui in basso (dopo la guida) la sezione "Tecnicamente". Se siete temerari o del settore, invece, segue anche la sezione "Approfondendo". Non c'è sezione più giusta o più sbagliata, spetta al singolo sapere quanto e *per* quanto vuole scavare a fondo; o quando fermarsi :)  

## Cosa fare
> ATTENZIONE: i procedimenti saranno indicati solo per PC per non rendere il contenuto troppo lungo

#### Download dati personali
Prima di tutto, richiediamo l'intera copia dei dati che Google ha sul nostro conto da qui: https://takeout.google.com/. Questo è molto importante perché ci aiuta a capire nel dettaglio quante cose sa Google, senza contare il fatto di avere una copia tutta nostra da conservare per bene. L'operazione potrebbe richiedere da alcuni minuti a un paio di giorni, a seconda di quanto e *da* quanto usate l'account (in media qualche ora, e non dovete tenere il PC acceso); quindi il nostro consiglio è di aspettare di avere la copia prima di continuare con il resto. Di darci una bella spulciata e poi di tornare qui :)

#### Lista attività
Una volta ottenuta la copia dei dati, iniziamo dall'eliminare la lista di attività dirigendoci [qua](https://myactivity.google.com/myactivity) e assicurandoci di essere connessi con l'account Google (in caso contrario, ci avviserà e non vedremo nessuna lista attività). In alto a destra troviamo 3 puntini: clicchiamoci e nel menù a scomparsa selezioniamo "Elimina attività per"<br>
<div align="center"><img src="resources/L1-0_gif0.gif"></div>  
Nella schermata seguente ci chiederà quali dati cancellare e da quando a quando. Sotto "Elimina per data" clicchiamo su "Oggi" e nel menù a scomparsa selezioniamo "Sempre" (ovvero "da sempre", tutti i dati). Al premere su "Elimina" il motore di ricerca ci chiederà più volte se siamo sicuri: confermiamo tutte le volte. <i>Poof</i>, attività cancellate! <br>
<div align="center"><img src="resources/L1-0_gif1.gif"></div>  

#### Tracciamento GPS
Per eliminare i nostri spostamenti invece, andiamo [qui](https://www.google.com/maps/timeline). Per renderci conto del dettaglio di informazione, vi invitiamo a cliccare su qualsiasi puntino rosso nella cartina: sulla sinistra apparirà la cronologia completa di quell'evento. Non si esagerava quando, nell'esempio di Carlo, si parlava di "ogni movimento tracciato".  

In basso a destra della cartina troveremo l'icona di un cestino. Premiamola, spuntiamo "Ho capito e voglio eliminare tutta la Cronologia delle posizioni" e infine clicchiamo su "Elimina Cronologia delle posizioni". Se ci dovesse chiedere ulteriori conferme, confermiamo. Tadaan!
<div align="center"><img src="resources/L1-0_gif2.gif"></div>  

#### Disabilitazione future raccolte dati
Quello che abbiamo fatto per ora è stato eliminare i dati *già* raccolti. Il prossimo passo è impedire che ne raccolga di nuovi. Andiamo nella nostra [Gestione Attività](https://myaccount.google.com/activitycontrols) Google. Qua troveremo un totale di 6 impostazioni che possiamo disabilitare. Viene da sé che si consiglia la disabilitazione di tutte quante, ma la scelta è prima di tutto vostra. Segue un riassunto delle opzioni.
- **Attività Web e app**: siti visitati, app aperte, cronologia ricerche, da dove, come, quando e per quanto tempo. In sintesi, la lista attività
- **Cronologia delle posizioni**: salvataggio spostamenti quando abilitate il GPS (il tracciamento di prima).
- **Informazioni del dispositivo**: info quali i contatti che avete in rubrica, eventi nel calendario, app installate, specifiche dispositivo
- **Attività vocale e audio**: salvataggio traccia audio di tutto ciò che dite con "Ok, Google" o, comunque, interagendo con il microfono Google
- **Cronologia delle ricerche di YouTube**: salvataggio di ciò che cercate su YouTube
- **Cronologia visualizzazioni di YouTube**: salvataggio di ciò che guardate/avete guardato su YouTube

Per disabilitarle, basterà cliccare sul bottoncino blu sulla destra di ogni opzione, che diventerà grigio. Google definirà l'attività "in pausa", ovvero disabilitata.<br>
Complimenti, in 10 minuti avete già arginato grandi quantità di dati, passati e futuri :)
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)
<br>

## Tecnicamente

Torniamo alla biblioteca: dato che la luce non gliela regala nessuno, deve guadagnare in qualche modo. Grazie a quei dati che ha raccolto, decide di offrire pubblicità personalizzata. Mette un annuncio in giro che recita così: "Avete un'attività? Lasciate volantini da me e li darò ai clienti più in linea con il vostro prodotto!"  
Se per esempio vogliamo vendere corsi di equitazione, noi paghiamo la biblioteca per lasciare i nostri volantini. E, poco prima che i clienti interessati ai cavalli si avvicinino per chiedere informazioni, la commessa fa apparire i volantini sul bancone. Si avvicina un appassionato di macchine? Ecco che appare un volantino di motori. Fin qui tutto ok. Se non fosse che stiamo parlando di Google nel 2000.  
  
2005: Google AdSense. La biblioteca assume persone per fare il trucchetto dei volantini... in altri negozi. Soprattutto chi ha appena iniziato e vuole fare "soldi facili" si rivolge alla biblioteca, che dividerà con loro i ricavati. Perché? Perché l'unica cosa chiesta in cambio è l'installazione di telecamere (della biblioteca) nel negozio. Insomma: la biblioteca inizia a ottenere informazioni dettagliate non solo più su chi entra nella biblioteca, ma anche su chi visita i negozi affiliati con essa. E al commerciante non gli costa nulla, almeno sul breve termine.  
Attenzione però: puntare il dito solo su Google in questo caso sarebbe ipocrita. Nel punto precedente si parlava di come sia il modello di business in generale a essere deleterio perché, alla fine, chi acconsente a questi mezzi senza fare nulla, sotto sotto non è diverso da chi li ha ideati⁵. Ricordiamoci che c'è sempre un'alternativa.

2005: Gmail. Vedremo questo aspetto nel dettaglio più avanti, ma per farla breve: la biblioteca instaura un sistema di posta gratuito. Perché? Perché quando inviate una lettera si prende la libertà di aprirla, leggerla, studiare ancora di più le vostre abitudini, richiuderla e inviarla al destinatario. Questo fino al 2017.

2006: Google acquista YouTube.

2007: Google acquista Android.

Potremmo continuare la lista con Google Drive, Google Foto, Google+ (ora defunto), Google Maps ecc. ma pensiamo di aver reso l'idea: ogni servizio Google ha lo scopo intrinseco di ottenere sempre più informazioni sul vostro conto, perché gli viene imposto dal modello di business che lui stesso ha ideato. Ironicamente, è come se fosse vittima di se stesso: se smette di fagocitare dati, inizia a risentirne.

Inoltre, mettetevi nei panni di qualcuno che voglia provare a competere con servizi del genere, per giunta gratuiti: di quanti nuovi social avete sentito parlare dal 2011? Quante alternative per YouTube? Quanti motori di ricerca che hanno un verbo a loro dedicato -googlare- usato quotidianamente da mezzo mondo?⁶  
Chiariamo, non è che nessuno abbia provato a fare nuovi social, siti per guardare video o motori di ricerca (vedremo tutto ciò lungo il percorso), ma la domanda è quanti ne conoscete; e quanti di questi non sono deserti.

<br>
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)

## Appendice
¹Entrate generate nel primo trimestre 2019: https://abc.xyz/investor/static/pdf/2019Q1_alphabet_earnings_release.pdf  
²Per dovere di cronaca, i "magheggi" di questo business non sono finiti qui (es. i cookie), ma parlarne in questa sezione sarebbe prematuro e creerebbe più confusione che altro.  
³https://www.brennancenter.org/sites/default/files/publications/2019_DHS-SocialMediaMonitoring_FINAL.pdf  
⁴Che per la cronaca, quello che fa è non lasciare tracce sul *vostro* computer. Chrome (e quindi Google) sa benissimo dove state andando. `¯\_(ツ)_/¯`  
⁵Si potrebbe approfondire per ore come strutturare un esercizio commerciale etico, ma non è questa la situazione adatta, in quanto il percorso si concentra sulle singole persone.  
⁶Mack Zachary, [*Big Tech's problem is its lack of competition*](https://www.theverge.com/2019/6/25/18744342/big-tech-competition-antitrust-regulation-amazon-apple-facebook-google-kara-swisher-vergecast), The Verge, 2019
