# Cookie: briciole di internet

Avete presente la storia di Pollicino che lascia le briciole per trovare la strada di casa? Bene, stiamo per parlare della stessa cosa, solo che non ci saranno uccelli a mangiarle e far sparire le tracce (quello lo impareremo nel prossimo capitolo). Questo è un capitolo d'introduzione, quindi basta leggere.  

Intanto, cos'è un cookie? Un cookie è un piccolo contenitore di informazioni che viene immagazzinato *nel vostro computer*.  
Facciamo un esempio: immaginate di entrare in un negozio in Italia dove i dipendenti parlano tante lingue. All'entrata c'è una pila di piccoli contenitori in vetro con dentro una bandierina italiana. "A cosa serve?" chiedete perplessi a un commesso. Lui ve ne porge uno, e vi spiega che in questo modo i suoi colleghi, che conoscono tante lingue, capiranno in che lingua comunicare con voi col solo guardare questo contenitore. Di portarlo con voi in giro per il negozio. Ancora più perplessi, chiedete: "E se parlassi inglese?". "Beh, semplice" risponde lui, "vi avrei sostituito la bandierina con quella dell'Inghilterra. E così per tutte le lingue che parliamo qui". "Se vuole" continua, "posso cambiargliela in ogni momento, se volete parlare in un'altra lingua. Basta che me lo dica."  
Così un giorno tornate, portate il vostro contenitore con voi e saltate la procedura. Perché già sanno che lingua parlate.  

Ovviamente un negozio del genere sarebbe parecchio assurdo, ma questo è quello che succede quando entriamo in un sito internet. E quel contenitore è un cookie. In questo caso, un cookie per la lingua che di base è in italiano, ma che potete cambiare a vostro piacimento (come ha spiegato il commesso). Ci sono tanti tipi di cookie, per esempio quelli che si ricordano del vostro accesso a un sito, che si ricordano le impostazioni grafiche e via dicendo. Quando entrate in un sito, insomma, vi vengono appioppati tanti piccoli contenitori (salvati nel vostro browser, come Chrome, Firefox ecc.) così che quando tornate, il sito si ricorda di voi guardando cosa c'è stato messo dentro. Un cookie quindi può essere molto utile se usato nel modo giusto. Ma, dato che il "modo giusto" per certe aziende è un concetto molto astratto (sigh), da lì ad usarli per monitorare gli utenti di siti non loro il passo è stato breve. Vediamo come.

## Terze parti

Il problema nasce dai cookie di terze parti. Che, come suggerisce il nome, non sono del sito bensì appartengono a qualcun altro.  
Sappiamo che un cookie contiene un'informazione: immaginate cosa potreste fare se potreste mettere un vostro cookie in più siti possibili. Così, quando una persona entra per la prima volta in uno di questi siti, voi lo etichettate con questo cookie, tipo "Utente001". E ogni volta che lui passerà in uno di questi siti che condividono il vostro cookie, saprete già che è "Utente001". Anche se cambia connessione, riavvia il PC, poco importa: il cookie è nel browser e finché navigherà con quel browser, saprete dove bazzica il caro "Utente001".  
Magari questa applicazione sembra poco pratica, ma rimediamo subito: facciamo che non mettiamo UN cookie, bensì *tanti* cookie su questi siti. Ognuno per una funzione specifica, che basterà poi incrociare per capire cose come quanti anni avete, da che paese state navigando, cosa vi piace; e la lista può continuare per molto: siti più visitati, condizione sociale, nucleo familiare, lingue parlate, acquisti fatti e così via. Su quanti più siti saranno questi cookie, tante più informazioni potrete ottenere da tutti gli utenti che passano di lì. E avete presente il detto: "L'informazione è potere"? Bene, tenete allora presente che nel 2015 i siti con Google Analytics (quindi con i cookie per l'analisi pubblicitaria di Google) erano stimati tra i 30 e i 50 *milioni*¹. Sapete quando vi appaiono pubblicità riguardanti gli ultimi siti che avete visitato? Perfetto, ora sapete il perché².

## Non so chi sei ma ti conosco

Questo porta a un altro problema: può un sito come Facebook conoscervi anche se non avete un account Facebook? La risposta è sì, e il modus operandi è sempre lo stesso: ogni volta che, per esempio, trovate su un sito un bottone per mettere "Like" su Facebook, quel sito sta ospitando un pezzo di Facebook. Certo, è comodo perché non dovete passare direttamente dal social network se volete mettere il like, ma al tempo stesso gli si sta dando "giurisdizione" sul sito, come se fosse un prolungamento di Facebook stesso. E tra le cose che può fare, mettere i propri cookie è una di quelle. E di conseguenza può profilarvi, anche se non avete mai messo piede su Facebook.

## Rimuovere le briciole

La notizia positiva c'è, ed è che ci sono estensioni per impedire tutto ciò. Certo, servirebbero leggi apposite (il GDPR non è riuscito molto su questo aspetto, se non a forzare i siti ad avvisarci che usano i cookie), delle regolamentazioni per evitare che queste cose accadano in primis, ma... nel frattempo, perché continuare a subire questi abusi? In questo livello impareremo come muoverci sul web lasciando meno tracce possibili.  
  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice
¹ McGee Matt, [As Google Analytics Turns 10, We Ask: How Many Websites Use It?](https://marketingland.com/as-google-analytics-turns-10-we-ask-how-many-websites-use-it-151892), Maketing Land, 2015  
² I cookie non sono l'unico fattore per profilare tuttavia: se vi ricordate infatti, nel livello 1 abbiamo detto a Google di non tracciare le nostre abitudini (video YT, GPS ecc.) e abbiamo smesso di usare la barra di Google come ulteriore precauzione (senza togliere agli altri motivi).
