# Libre Office  
  
> Windows è il sistema operativo più utilizzato e, con esso, la sua suite Office. Un'inchiesta del governo olandese tuttavia, ne ha dimostrato l'alta invadenza

## In parole semplici  

LibreOffice è una variante libera, grauita e *open source* dei principali strumenti del pacchetto Office di Microsoft (word, excel e powerpoint). Quest'ultimo, come detto nei primi capitoli, è stato trovato a inviare telemetria non disattivabile (dati) ai suoi server¹, come per esempio i destinatari delle mail, frasi sistemate col correttore, frasi tradotte e, più in generale, metadati (quelli che sfrutta WhatsApp, se vi ricordate). Per fare un paragone, Windows 10 in sé invia 1.200 tipi di dati a 10 team diversi. La suite Office 365 ne invia *25.000*. Più di 20 volte tanto, rendendola a tutti gli effetti un pozzo di petrolio di raccolta dati (a pagamento). 

Comportamenti simili hanno spinto la Germania a bandire dalle scuole questa suite nel luglio 2019, reputandola un pericolo per la privacy². Oltre le scuole, anche altre istituzioni pubbliche come l'associazione federale IT tedesca che si occupa dei servizi municipali si è mostrata preoccupata in merito, dicendo di star prendendo in considerazione alternative *open source* e che bisognerebbe riservare dei fondi per questi strumenti. Soprattutto se si considera che nel 2018 in Germania hanno speso circa 73 milioni di euro per le licenze Microsoft³.  

Sul lato tecnico, c'è invece chi potrebbe dire che a livello di prestazioni LibreOffice non raggiunga quelle di Microsoft. Ma, a meno che non abbiate file da centinaia e centinaia di MB l'uno, il programma non da problema alcuno. Sottolineiamo inoltre che, oltre ad avere un formato tutto suo, apre tutti i file Word, Excel ecc. fatti col pacchetto Microsoft, quindi non ci sono problemi di compatibilità. Infine, potreste aver sentito parlare di un'altra alternativa *open source* chiamata OpenOffice, ma dato che non viene aggiornata da anni, ve la sconsigliamo.  

Insomma: se volete evitare l'ennesimo programma di raccolta dati e al tempo stesso risparmiare soldi, l'alternativa esiste ed è perfettamente legale :)


## Cosa fare  
Beh... 
- scaricatelo dal sito ufficiale => https://www.libreoffice.org/download/download/
- installatelo
- fine!

Inoltre, se siete i responsabili di un qualsivoglia ufficio, potreste considerare l'idea di passare a LibreOffice "business" (più info [qui](https://www.libreoffice.org/download/libreoffice-in-business/)), risparmiando sulle spese delle licenze. E di quei soldi risparmiati, riservarne una fetta per supportare gli sviluppatori del programma, così che possano renderlo sempre più prestante e risparmiare a tutti telemetria invadente: https://www.libreoffice.org/donate/  
Un guadagno per entrambi :)  

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)   

> aggiungere un "Tecnicamente" in questo capitolo sembra abbastanza futile, dato che quello che c'era da dire è già stato detto

## Appendice
¹ Cimpanu Catalin, [Dutch government report says Microsoft Office telemetry collection breaks GDPR](https://www.zdnet.com/article/dutch-government-report-says-microsoft-office-telemetry-collection-breaks-gdpr/), ZDNet, 2018  
² Schaer Cathrin, [Microsoft Office 365: Banned in German schools over privacy fears](https://www.zdnet.com/article/microsoft-office-365-banned-in-german-schools-over-privacy-fears/), ZDNet, 2019  
³ Krempl Stefan, [73 Millionen Euro pro Jahr: Kosten für Microsoft-Lizenzen beim Bund steigen](https://www.heise.de/newsticker/meldung/73-Millionen-Euro-pro-Jahr-Kosten-fuer-Microsoft-Lizenzen-beim-Bund-steigen-4466370.html), Heise, 2019
