# Non nel mio nome pt.1: F-Droid, lo store trasparente

> Se pensavate che Play Store e Apple Store fossero gli unici posti dove scaricare applicazioni, è tempo di ricredervi: F-Droid nasce con l'intento di promuovere app senza traccianti (come i cookie) e open source

## In parole semplici

Prima di avventurarci tra i vari siti e i loro cookie, dobbiamo introdurre F-Droid, lo store "di fiducia" (letteralmente). "Di fiducia", perché per apparire su F-Droid le app vengono controllate manualmente e devono rispettare una regola essenziale: devono essere FOSS (*Free Open Source Software*, software libero e open source). Questo vuol dire che nessuna app su F-Droid può essere a pagamento, contenere pubblicità e essere closed source (quindi non verificabile). Se un autore vuole soldi, può optare inserendo un link di donazioni nella pagina dell'app oppure usare metodi tipo: "con la versione gratis hai X, ma se paghi ti diamo anche Y" (in gergo tecnico *freemium*).  

Riguardo i traccianti, se l'app ne fa uso troveremo nella descrizione il seguente avviso: "Questa app monitora e riferisce sulle tue attività". Più in generale, ci sono una serie di "anti-caratteristiche" che fanno scattare un messaggio di allerta, molto più visibile, quando andiamo sulla pagina dell'app¹.  
<div align="center"><img src="resources/L2-1_pic0.png"></div>  

In questo modo siamo sempre informati di cosa stiamo andando a scaricare. Il tutto è possibile grazie al fatto che questi avvisi non sono automatici, bensì inseriti manualmente; perché ogni app viene controllata da persone in carne e ossa e rodata prima di finire su F-Droid (o *non* finire). C'è, insomma, un controllo rigido.  

In futuro useremo quindi F-Droid per scaricare alcune app (non disponibili sugli store normali) in sostituzione di altre app a noi sì più familiari, ma -come dimostreremo- meno etiche. Questo ci garantirà maggior trasparenza su cosa stiamo scaricando, minor dipendenza da grandi servizi opachi come il Play Store, e di conseguenza un telefono esposto a meno rischi. Sempre meno legnetti :)  

## Cosa fare

#### Installazione

Dato che rappresenta la competizione, non troverete l'app di F-Droid sugli store classici. Per scaricarlo, dovete andare dal vostro telefono su https://www.f-droid.org/ e premere il bottone "Scarica F-Droid" ben visibile al centro.  
  
Una volta scaricato, il vostro telefono vi chiederà probabilmente se volete autorizzare il browser che avete usato (tipo DuckDuckGo) a installare app. Accettate e installate F-Droid.  
  
#### Telegram, ma FOSS

Su F-Droid troverete Telegram, ma non è lo stesso Telegram che avete installato sul telefono: la versione FOSS (libera e open source) si distingue perché le sono state rimosse le dipendenze da servizi proprietari (ovvero non liberi, ovvero che non sapete cosa fanno, ovvero da Google²): per essere considerata "libera" al 100% infatti, un'app dovrebbe appoggiarsi esclusivamente a servizi altrettanto liberi. Per essere completamente trasparente.  

Quindi molto semplicemente, disinstallate Telegram dal telefono (i messaggi rimangono, non preoccupatevi) e reinstallatelo cercandolo su F-Droid :)  
È giusto specificare che gli aggiornamenti rispetto all'app standard arrivano con qualche giorno di ritardo: questo perché i volontari che lavorano sulla versione FOSS devono 1) aspettare che venga rilasciata la versione ufficiale, 2) avere il tempo per ripulirla. E anzi, dato che sono volontari, ricordatevi che supportare queste persone è un gesto molto importante e molto apprezzato.

#### Dimmi che app sei e ti dirò quanti traccianti hai

Un'altra app molto interessante che vi consigliamo è **ClassyShark3xodus** (ha l'icona di uno squalo). Il suo ruolo è dirvi quanti *tracker* (traccianti) ha un'app e usarla è facilissimo: vi basta aprirla e selezionare, dalla lista che vi apparirà, l'app che volete analizzare; farà tutto in automatico. Vi consigliamo quest'esercizio per rendervi conto di quanti tracker risiedano nelle app che usiamo ogni giorno: eccovi un esempio sull'app di *bike sharing* MoBike.  
<div align="center"><img src="resources/L2-1_pic2.png"></div>  

Abbiamo evidenziato in verde il nome dell'app, in azzurro i tracker trovati (4) e in arancione quali: in questo caso sappiamo che l'app si appoggia non solo ai servizi pubblicitari di Google (CrashLytics e Firebase Analytics) ma usa anche tracker di Baidu, il "Google" cinese famoso per la censura a contenuti come Piazza Tiananmen 1989. Insomma: *green* certo, ma etico...  

<br>

A questo punto quindi, avete 2 store sul telefono: uno ufficiale, per potervi comunque concedere qualsiasi sfizio, e un altro libero, con ovviamente molta meno scelta ma al tempo stesso del tutto trasparente per andare sul sicuro. E se proprio volete capire la trasparenza di un'app scaricata dallo store ufficiale, vi basta controllarla con lo squalo di fiducia (che non vi dirà esattamente cosa fa, ma almeno chi e cosa traccia).  Vi lasciamo pasticciare un po' :)
  
Compito consigliato prima di passare al prossimo capitolo: controllare almeno 3 app che avete sul telefono con ClassyShark3xodus.
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md) 

## Tecnicamente
[DA AGGIUNGERE]  

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice
¹ Lista (in inglese) delle anti-caratteristiche: https://www.f-droid.org/it/docs/Anti-Features/  
² Per esempio il GCM (Google Cloud Messaging) che comunica a Google quando ricevete un messaggio, o la rimozione di Google Maps per condividere la posizione, sostituito da OpenStreetMap  
