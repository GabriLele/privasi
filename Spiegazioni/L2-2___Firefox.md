 # Non nel mio nome pt.2: ~~Chrome~~ Bromite e Firefox, finestre della rete

> Google Chrome è il browser più usato al mondo, con 5 miliardi di download sui soli telefoni¹. Quanto rispetta la nostra sfera privata però? Purtroppo ben poco

## In parole semplici

Immaginate di guardare fuori da una finestra. Questa può avere delle persiane, e in quel caso vedrete solo delle sottili strisce. Oppure può avere i vetri chiusi e azzurri, così tutto ciò che vedrete sarà tendente all'azzurro. Una macchia sul vetro invece non vi farà vedere bene in quel punto. Insomma: se volete vedere cosa c'è fuori dalla finestra, dovete prima di tutto assicurarvi che la finestra vi permetta di vederci bene.  
  
E cosa sono alla fine i *browser* (quindi Chrome, Firefox, Edge e altri) se non finestre che affacciano sulla rete? Se ci pensiamo, per aprire un sito internet passiamo dal browser. Tutti i posti in cui navighiamo, gli spazi online in cui scriviamo, passano tutti dalla finestra del browser. Il browser è quindi una vera e propria finestra digitale.  

Quello che abbiamo fatto finora è stato lavorare a ciò che già avevamo visto dalla finestra. Per l'esattezza a rimuovere vecchi contenuti (quindi alla finestra già noti), come vecchi account. Non abbiamo mai creato nuovi account, nuove mail né altro. E per un semplice motivo: se la finestra tiene traccia di ciò che facciamo, prima va cambiata la finestra.  

Uno potrebbe pensare: "Perché una finestra, che dovrebbe servire a guardare fuori senza far entrare cose dall'esterno come gli insetti, dovrebbe spiare quello che faccio?". Ed è una bella domanda, alla quale però già conoscete la risposta: conoscerci meglio per guadagnare di più. Un giornalista del Washington Post, per esempio, ha analizzato nel 2019 il comportamento di Google Chrome e quello di Mozilla Firefox. Risultato? In una settimana Chrome aveva autorizzato più di *undicimila* richieste di cookie, Firefox zero². In altre parole, ricostruire i movimenti (e di conseguenza i comportamenti, il modo di essere) del giornalista era ridicolissimamente facile.

Uno poi potrebbe dire che, anche se cambiamo la finestra, come si può essere certi che chi sta fuori dalla finestra (i siti) non ci spii a sua volta? Beh, nella vita reale mettiamo delle tende e il problema è risolto (senza contare che chi vuole vendervi dei prodotti non vi viene certo a spiare dalla finestra ahah). In quella digitale la logica è la stessa, solo che quelle tende si chiamano "*add-ons*" (estensioni).  

Tempo di bricolage e arredi!  

## Cosa fare

#### Telefono: Bromite

Iniziamo dal telefono con Bromite. Bromite è una versione di Chrominium (progetto sul quale si base il più famoso Chrome) il cui scopo sarà quello di proteggere la vostra privacy. Vi ricordate di quando vi abbiamo fatto installare Duck Duck Go sul telefono? Tempo di disinstallarla e fare level up :D da ora in poi useremo Bromite per cercare su internet. Gli sviluppatori distribuiscono il browser per conto proprio utilizzando F-Droid come store. Vediamo come fare per aggiungerlo sul telefono.

- Disinstallate DuckDuckGo
- Andate su F-Droid
- Aprite la finestra "Impostazioni" in basso a destra
- Premete su "Repository" e successivamente sul "+" in alto a destra
- Inserite ```https://fdroid.bromite.org/fdroid/repo``` come indirizzo di repository e (opzionalmente) ```E1EE5CD076D7B0DC84CB2B45FB78B86DF2EB39A3B6C56BA3DC292A5E0C3B9504``` come impronta digitale

<div align="center"><img src="resources/L2-2_pic0.png"></div>  

Adesso Bromite sarà tra le applicazioni gestite da F-Droid, cercatela e installatela.
Prima di iniziare ad utilizzare il browser andiamo a modificare qualche impostazione per rendere l'app ancora più discreta: apriamo Bromite, premiamo sui 3 puntini in alto a destra della schermata base di Bromite e selezioniamo "Impostazioni".

<div align="center"><img src="resources/L2-2_pic1.png"></div>  

- In "Motore di ricerca" selezionate DuckDuckGo.
- In “Pagina iniziale” impostate su OFF
- In "Privacy" abilitate “Open links in incognito tabs always“ per utilizzare la navigazione in incognito automaticamente.

Se vi scordate di chiudere tutte le vostre schede di navigazione, nessun problema: ci sarà una notifica a ricordarvi che avete lasciato aperto qualche sito. Vi basterà premerci sopra e cancellerà tutto per voi.

<div align="center"><img src="resources/L2-2_pic2.png"></div>  

#### PC: Download e impostazioni base

Qui le cose si fanno più impegnative, ma non scoraggiatevi. Tuttavia, finché non avete completato TUTTI i passaggi, NON usate Firefox e continuate col vostro vecchio browser.  
  
Prima di tutto scarichiamo e installiamo Firefox dal sito ufficiale: https://www.mozilla.org/it/firefox/new/  
Fatto ciò, apriamolo e andiamo nelle Opzioni. Ci sono due modi per farlo:
- premere sulle 3 lineette in alto a destra e, dal menù a scomparsa che si apre, su "Opzioni"
- scrivere ```about:preferences```  nella barra di ricerca e premere invio

Selezionate sulla sinistra "Privacy e sicurezza" e impostate quanto segue:
- Blocco Contenuti: restrittivo (blocca più traccianti possibili di base)
- Cronologia: in "Impostazioni cronologia" selezionate "utilizza impostazioni personalizzate" e spuntate "Utilizza sempre la modalità Navigazione Anonima" (vi chiederà di riavviare Firefox, fatelo)

*Se il PC è solo vostro*, tornate nelle opzioni e in "Avvio" premete su "Imposta come browser predefinito". Vi porterà alle impostazioni del computer e da lì sostituite Edge/Chrome con Firefox. Abbiamo detto SE è solo vostro, perché dare una doccia fredda di "Tiè, ora fai come faccio io" alle altre persone non è proprio la migliore delle soluzioni (ricordate che voi in primis non siete arrivati qui da un giorno all'altro).  

  
#### Estensioni  

La finestra c'è, ma avventurarsi per internet non è ancora il massimo. Prima di aprire qualsiasi sito, è fondamentale mettere le tende. Apriamo Firefox e andiamo sullo store delle estensioni di Mozilla: https://addons.mozilla.org/it/firefox/.  

##### uBlock Origin

Per cercare un'estensione, vi basta digitare il suo nome nella barra in alto a destra come illustrato qui sotto. Iniziamo da uBlock Origin

<div align="center"><img src="resources/L2-2_gif0.gif"></div>  

Parlando di docce fredde, questa è probabilmente la doccia del capitolo: con uBlock Origin andremo a bloccare tutte le richieste *di terze parti* e, se proprio il sito non vuol saperne di andare, le abiliteremo sul sito in questione.  
  
Durante l'installazione ogni estensione vi chiederà se volete attivarla anche per la modalità in incognito: mettete sempre la spunta di conferma o non funzionerà.  
  
<div align="center"><img src="resources/L2-2_pic3.png"></div>  

Una volta installata, l'obiettivo è corazzarla come da GIF.  

<div align="center"><img src="resources/L2-2_gif1.gif"></div>  

Abbiamo in poche parole bloccato l'accesso a tutte le terze parti (le caselline rosse), a javascript (un codice di programmazione che rende i siti più interattivi ma al tempo stesso più vulnerabili) e ad altre piccole funzioni.  

E se il sito non funziona come dovrebbe? Finché si tratta di qualche impostazione grafica, vi consigliamo di passarci sopra. Se invece a non funzionare sono cose necessarie alla navigazione e non masticate tecnologia potete risolvere in due semplici passaggi (per la spiegazione avanzata e più sicura invece, consultare "Tecnicamente"):
- dal sito in questione cliccate sull'iconcina di uBlock Origin e riabilitate javascript cliccando 1) sull'iconcina sbarrata in basso a destra 2) sul lucchetto a sinistra per ricordare le preferenze 3) sul refresh (le freccette in cerchio)
- se continuasse a dar problemi, riaprite l'iconcina e cliccate *al centro* delle casellina rossa *opaca* più in alto, che diventerà grigia. Di nuovo lucchetto e refresh  

<div align="center"><img src="resources/L2-2_gif2.gif"></div>  

Così facendo avete praticamente disabilitato uBlock su quel sito. In alcuni siti non succede nulla, in altri sì, ma meglio farsi male da una parte che da tutte le parti. Per esempio, GitLab (questo sito) richiederà entrambi i passaggi per leggere i capitoli ma non manda comunque richieste a siti esterni.  

Bene, se siete sopravvissuti fin qui, il resto è una passeggiata :)  

##### Cookie AutoDelete

Volete buttare via i contenitori appena uscite dal negozio? Questo è quello che fa Cookie AutoDelete, ogni volta che cambiamo sito o chiudiamo una scheda internet. Quindi accettate pure tutti i cookie che volete, tanto andranno poco lontano.  

> ATTENZIONE: non tenete aperti più siti dove accettate i cookie e/o con uBlock Origin disabilitato, o sarà possibile a quei siti fare 2+2. **Prendete l'abitudine di chiudere le schede quando non vi servono più!**

Cercatelo sullo store come avete fatto con uBlock Origin, installatelo e rinforzatelo come segue:

<div align="center"><img src="resources/L2-2_gif3.gif"></div>  

##### HTTPS everywhere

Avrete sicuramente notato all'inizio dei link le sigle HTTP e HTTPS. Queste sigle hanno il compito di metterci in contatto con i siti, come un vero e proprio ponte. La differenza tra le due è che HTTPS è più sicura (la S sta per Secure, sicuro in inglese), perché quello che fa è crittare la comunicazione. Molti siti dispongono di entrambe le versioni, e questa estensione forza l'HTTPS. Installatela e farà tutto per voi.

##### LocalCDN

> Attenzione, ci sono due versioni di due autori diversi: una aggiornata e una no. Stiamo parlando di quella di nobody42, ovvero [questa](https://addons.mozilla.org/en-US/firefox/addon/localcdn-fork-of-decentraleyes/)

LocalCDN evita che certi contenuti necessari per far andare un sito vengano caricati da terze parti come Google, usando invece una versione creata ad hoc in locale (per non rompere il sito). CDN sta infatti per Content Delivery Network ("rete di consegna contenuti") e se volete capire meglio come funzionano, trovate qui in basso in "Tecnicamente" una spiegazione approfondita. Per quanto riguarda l'estensione, ci basta installarla senza fare nient'altro.

##### CanvasBlocker

CanvasBlocker evita il  "cookie 2.0", il *fingerprinting* (letteralmente "prendere le impronte digitali"). Questo sistema al posto di lasciarvi i cookie nel browser, traccia le caratteristiche dell'intera finestra (come se appunto ne prendesse le impronte). Quando un sito ci prova, CanvasBlocker gli ritorna un dato fasullo. Ops :)  
Come le altre, vi basta installarla.

##### ClearURLs
CleaURLs sarà il vostro spazzino di fiducia rimuovendo per voi i parametri traccianti dai link che clickerete. Addio ai link inutilmente lunghi il cui unico scopo è quello di seguire la navigazione online.
Basta installarlo e farà tutto da sé. 

<br>  

La vera sfida del capitolo è abituarsi un po' al tutto. Perché ovviamente, se fate il login su un sito e Cookie AutoDelete vi cancella il cookie di accesso appena chiudete la finestra, ogni volta dovrete riloggare. Inizialmente può essere scomodo, ma sulla lunga può per esempio aiutarvi a rendervi conto di quali siti vi importa davvero e quali aprite per noia. Qualcuno invece potrebbe pensare che è assurdo fare "tutti" questi salti mortali (come uBlock Origin), ma vi invitiamo a riflettere sul fatto che questi salti mortali sono stati resi necessari proprio per come internet funziona attualmente, ovvero un'enorme macchina di pubblicità e dati. Ciò che dovrebbe indignare quindi, non è la "pretesa" a farvi installare questo e quest'altro anche solo per aprire un link, bensì il come le grandi compagnie di internet che circondano le nostre vite (Google, Facebook, Amazon, Twitter, Reddit e via dicendo) ci abbiano costretti a simili contromisure per non essere costantemente monitorati per i loro profitti. Vi dà fastidio? Dovrebbe. Ma prendetevela con loro, non con noi.

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)
  
## Tecnicamente

[DA AGGIUNGERE: in depth uBlock, WebRTC, cos'è un CDN]  
  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)

## Appendice

¹ Verma Kashish, [Google Chrome pasts 5 billion downloads; a huge lead over Mozilla Firefox](https://thegeekherald.com/p/google-chrome-pasts-5-billion-downloads-a-huge-lead-over-mozilla-firefox/), The Geek Herald, 2019  
² Fowler Geoffrey A., [Goodbye, Chrome: Google’s Web browser has become spy software](https://www.washingtonpost.com/technology/2019/06/21/google-chrome-has-become-surveillance-software-its-time-switch/), The Washington Post, 2019
